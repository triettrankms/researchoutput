## What for
   * Building Docker images from code
   * The `docker build` command builds an image from a `Dockerfile` and a `context`
   * `context` of a build is a set of files at specified location local directory `PATH` or Git repo/tar ball `URL`

## Mechanism
   * Context is sent to daemon. Files inside the context can be referenced to by instructions in the Dockerfile
   * Instructions are run one-by-one from top to bottom
   * Side effects are retained
   * Results of each step might be **run** inside **intermidate** containers and **commited** into intermediate **images**

## Structure

```
#comments
# parser directives if any

#Build stage 1
FROM ARG
INSTRUCTION argument

#Build stage 2
FROM ARG
INSTRUCTION argument

#Build stage 3
FROM ARG
INSTRUCTION argument

```

## .dockerignore
   * like .gitignore => Excludes files/directories from context
   * **Note** `foo/bar` and `/foo/bar` ignore the same file bar inside directory foo inside the root context 
## Instructions

FROM <image>[:<tag>] [AS <name>]
  
  | Instruction | Usage | Common form
  | ----------- | ----- | -----------
  | `FROM` | sets the base image to use for the current build stage | `FROM <image>[:<tag>] [AS <name>]`
  | `ENV` | sets environment variables for the current build stage; these can also be accessed from inside the container when it starts |  `ENV <key>=<vavlue>`
  | `RUN` | executes commands in a **new layer** and **commits results**; resulting committed image is used for next step in the Dockerfile; | `RUN ["executable","param1","param2"]` 
 | `CMD` | provides defaults for an executing container |  `CMD ["executable","param1","param2"]` (**default executable**) `CMD ["param1","param2"]` (**default parameters for `ENTRYPOINT`**)
 | `LABEL` | adds metadata to an image; parent images' labels are inherited | `LABEL <key>=<value>` |
 | `EXPOSE` | informs Docker that the container listens on the specified network ports at runtime | `EXPOSE <port> [<port>/<protocol>...]`
 | `ADD` | copies files from **build context** or **remote URL** to image's filesystem | `ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]`
 | `COPY` | copies files from **build context** or **previous build stage** to image's filesystem  | `COPY [--chown=<user>:<group>] ["<src>",... "<dest>"`
 | `ENTRYPOINT` | allows configuring a container that will run as an executable | `ENTRYPOINT ["executable", "param1", "param2"]`
 | `USER` | sets UID/GID to use when running the image for any **preceding** `RUN`, `CMD`, `ENTRYPOINT` | `USER <user>[:<group>]` `USER <UID>[:<GID>]`
  | `WORKDIR` |  sets the working directory for any **preceding** `RUN`, `CMD`, `ENTRYPOINT`, `COPY`, `ADD` instructions | `WORKDIR </path/to/workdir>`
  | `ARG` | defines a variable which can be passed in at build-time  | `ARG <name>=<default value>`

 
 **Note/Source of bug**

 * Both `CMD` and `RUN`: 
    * `RUN/CMD ["echo","$HOME"]` won't do shell expansions but `RUN/CMD [ "sh", "-c", "echo $HOME" ]` does
    * Blacklashes need escaping
    * Parameters must be inside **(") not (')**
 * `CMD` usage:
    * When used to provide **default argument** for `ENTRYPOINT`
 * `ENV`:
    * Inside a Dockerfile, an environment variable's scope is its current build stage (next build stage can't refer to it)
    * > Environment variable substitution will use the same value for each variable throughout the entire instruction
    ```
    ENV abc=hello
    ENV abc=bye def=$abc
    ENV ghi=$abc
    ```
    results in `def` = `hello` and `ghi` = `bye`
* `ARG`:
    * Prior to its definition by an ARG instruction, any use of a variable results in an empty string
* `EXPOSE`:
  * > Does not actually publish the port
  * It is meant to be a documentation between people building images and running containners
  * > To actually publish the port when running the container, use the -p flag on docker run to publish and map one or more ports
* `ENTRYPOINT`:
  * Refer to the [`ENTRYPOINT` document](https://docs.docker.com/engine/reference/builder/#entrypoint) for further issues regarding kernel SIGNALs and differences between running executable in/without a shell
    
### `ENTRYPOINT` vs `CMD`
   * At least one of them is specified in a Dockerfile
   * When container should be used as an executable => use `ENTRYPOINT`
   * `CMD` should be used to define default arguments for `ENTRYPOINT` or running an ad-hoc command in the container
   * `CMD` arguments will be overridden with **run-time commandline arguments** when starting containers


### `RUN` vs `CMD`
  * `RUN`: actually runs a command and commits the result
  * `CMD`: does not execute anything at build time, but specifies the intended command for the image