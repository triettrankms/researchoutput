## Useful Links

1. [Getting started with Jenkins](https://jenkins.io/doc/book/pipeline/getting-started/) Useful links/tools
1. [Using a Jenkinsfile](https://jenkins.io/doc/book/pipeline/jenkinsfile/) details on using credentials in Jenkinsfile
1. [Jenkinsfile example](https://github.com/jenkinsci/pipeline-examples)

## Useful Tools

1. Global Variable Details: \<jenkins-server-url\>/pipeline-syntax
1. Snippet Generator (only generate steps): \<jenkins-server-url\>/pipeline-syntax
1. Declarative Directive Generator: \<jenkins-server-url\>/directive-generator


## Global Variable
1. Environment variables accessible from scripted pipeline: * **env**: 
1. Parameters defined for the pipeline: * **params**: 
1. Information about currently executing pipeline: * **currentBuild**: 

## Declarative Jenkinsfile Structure
  ```
  pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building..'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
} 
  ```
  **Directives meaning simplified**
  
  | Directive | Meaning 
  | --------- | --------- 
  | `pipeline`  | Start of pipeline
  | `agent` | Where to do ~ what executor/workspace
  | `stage` | What to do ~ map to CI/CD workflow
  | `step` | How to do ~ application specific 

## Jenkins syntax

1. Top level is a `pipeline {}` block which in turns consists of other blocks
1. Blocks must be one of **Sections**, **Directives**, **Steps** or **Assignment Statement**
1. Variables (parameters, variables...) are accessible via `${params.<PARAM_NAME>}` or `${env.<ENV_NAME>}`

### Sections

| Section | Meaning | parameter/stage/step... | Notes
| ------- | ------- | ------- | -------
| `agent` | which host to execute something | `any` `none` `docker` `dockerfile` | pass args into these steps (ex. into `docker run`); allowed in `pipeline` `stage`
| `post` | what to do after something is executed | `always` `changed` `fixed` `success` `failure` `unstable`| allowed in `pipeline` `stage` 
| `stages`  | contains 1 or more of `stage` | `stage` | allowed **Only once**, in `pipeline`
| `steps` | contains 1 or more steps | arbitrary steps | allowed in `stage` |

### Directives

| Directive | Meaning | parameter/helpers/step... | Notes
| ------- | ------- | ------- | ------- 
| `environment` | key-value pairs for reference by stages/steps | assignment statement |  allowed in `pipeline` `stage`; credentials should be referenced with `credentials()` helper inside this directive (instead of `parameters`)
| `options` (`pipeline` level) | configures the whole pipeline | `timeout` `buildDiscarder` `newContainerPerStage` | alowed **Only once**
| `options` (`stage` level) | configures a stage | `timeout` `retry` | steps in this directive are invoked before entering the `agent` or checkig any `when` conditions  
| `parameters` | parameter users provides when triggering the pipeline  | `string` `text` `booleanParam` `choice` `file` `password` | allowed **Only once**, in `pipeline`
| `triggers` | automatically re-triggers the pipeline | `cron` `pollSCM` | pipelines integrated with **GitHub** or **BitBucket** might not this since they have webhooks
| `stage` | typically a stage in a CI/CD workflow | name of the stage `agent` `post` | allowed in `stages` |
| `tools` | tools to auto-install and put on the `PATH` | specific steps to install supported tools ( **maven**, **jdk** and **gradle**) | ignored if `agent node`; allowed in `pipeline`
| `input` | prompt user for input using `input` step | `message` `id` `submitter` `parameters` | `stage` will pause after `options` and before entering `stage`'s `agent` or evaluating its `when`; if `input` is approved, `stage` continues; `parameters` provided will be available within the `stage`; allowed in `stage`
| `when` | determine whether `stage` should be executed depending on a condition | `branch` `changelog` `changeRequest` `environment` `equal` `expression` `not` `allOf` `anyOf` `beforeAgent` | `expression` is arbitrary *Groovy* expression; allowed in `stage`

### Steps

1. The list of steps is [here](https://jenkins.io/doc/pipeline/steps/).
1. [Basic steps](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps/)

### Note

1. **Pipeline** project, `when` with `branch` must use the following syntax
```
when {
  expression {
    return sh(script: "git rev-parse --abbrev-ref HEAD", returnStdout: true).trim() == "develop"
  }
}
```