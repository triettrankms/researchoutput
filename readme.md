# Research Outputs

This repository contains **slides**, **links to demo repos** and **other notes** I prepared and/or used to present my findings on the Linux system and DevOps stack to my team.

The files are listed in the order of research/presentation.

1. Linux:   
      * Linux Intro ~ *core concepts*
      * Bash Scripting and Firewall Config ~ *Shell/Bash scripting and ipconfig*
2. Jenkins:
      * Jenkinsfile
3. Docker:
      * Docker ~ *core concepts*
      * Dockerfile
      * Docker_Intro_Cont ~ *Docker networking, data storage, Docker Compose and Swarm*
      * [Docker project to provision simple 3-tier webservice](https://bitbucket.org/triet96/docker_demo)
4. Kubernetes
      * K8s_Intro ~ *core concepts*
      * K8s_Intro_Cont ~ *volume, deployment, ingress*
5. Ansible
      * [Ansible project to provision AWS ec2 instances and K8s cluster on top of AWS ec2](https://bitbucket.org/triettrankms/ansible_provision_k8s/src/master/)
6. Chef
      * [Chef project to provision K8s cluster on top of AWS ec2](https://bitbucket.org/triettrankms/chef-provision-k8s-cluster/src/master/)
      * Chef vs Ansible and Guide: ~ *comparison of 2 configuration management tools on facts and opinions; guide for core concepts/workflow with 2 tools*
